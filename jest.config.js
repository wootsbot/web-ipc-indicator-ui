module.exports = {
  collectCoverageFrom: [
    'app/**/*.{js,jsx,mjs}',
    '!app/pages/_*.js',
    '!config/**',
    '!coverage/**',
    '!app/.next/**',
    '!app/utils/**',
    '!app/pages/Dashboard.jsx',
    '!app/containers/Login/**',
    '!app/containers/LoginProvider/**',
  ],
  testPathIgnorePatterns: [
    '<rootDir>/app/.next',
    '<rootDir>/node_modules/',
    '<rootDir>/config/',
    '<rootDir>/coverage/',
    '<rootDir>/app/utils/',
    '<rootDir>/app/pages/Dashboard.jsx',
    '<rootDir>/app/containers/Login/',
    '<rootDir>/app/containers/LoginProvider/',
  ],
  coverageThreshold: {
    global: {
      statements: 90,
      branches: 90,
      functions: 90,
      lines: 90,
    },
  },
  moduleDirectories: ['node_modules', 'app'],
  transform: {
    '^.+\\.(js|jsx|mjs)$': '<rootDir>/node_modules/babel-jest',
    '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
    '^(?!.*\\.(js|jsx|mjs|css|json)$)':
      '<rootDir>/config/jest/fileTransform.js',
  },
  setupFiles: ['<rootDir>/config/jest/enzyme-setup.js'],
  testRegex: 'tests/.*\\.test\\.js$',
  testURL: 'http://localhost:3000',
};
