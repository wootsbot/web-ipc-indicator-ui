## GBM

### Por que use firebase functions y no la api GBM

[Codigo del proyecto](https://gitlab.com/wootsbot/web-ipc-indicator-functions)

Basicamente quise darle un poco de plus al proyecto usando las firebase fucntions para poder tener persistencia de los datos en cloud firestore de firebase.

### Por que nextJS ?

[nextJS](https://nextjs.org/)

Si tuberamos que poner en produccion esta pequeña aplicacion o reto, tendria motivos por los cual usar este Framewordk para react, hay muchas empresas lideres que usan next en llas ponemos encontrar a github, nexflix, docker, AuthO, entre otras.

puntos que a considerar son code splitting y reloading y universal rendering, podemos personalizar completamente su comportamiento como control total sobre Babel y Webpack, server personalizable y que hay de ponerlo en produccion optimizado para el tamaño de compilación muy pequeño, compilación para nosotros los desarrolladores más rápida. me prodria pasar habalando de las ventajas de nextJS pero quisas son unas de las razones por las cuales escoji nextJs para este ejemplo el cual se adpta muy bien.

### material-ui

[material-ui](https://material-ui.com/)

Si ya eres un betarano con react, sabras que material-ui es unos de los frameworks UI mas completos y con un poco de pacioencia, parctica pronto lo dominaras, tiene una apmplia cominidad para ayudarnos con los diferentes problemas que podamaos encontrar al momento de implementarlo, aparte tenemos ya la guia de stilos que google materil dicata para una mejor interfas de usuario.

### Firebase Auth

[Firebase](https://firebase.google.com)

Por que eleji la Authentication de firease, basicamente Administra tus usuarios de manera simple y segura lo cual tambien ofrece varios métodos para autenticar, tales como correo electrónico y contraseña, proveedores externos como Google o Facebook.

## Manos a la obra

Asegúrese de tener una versión reciente de [nodejs](https://nodejs.org) y la
herramienta de administración de dependencias [yarn](https://yarnpkg.com).

```sh
$ npm install --global yarn
```

Enseguida procedemos a instalar las dependencias del proyecto.

```sh
$ cd web-ipc-indicator-ui/
$ yarn
```

Y finalmente, ejecutemos el servidor en modo desarrollo:

```sh
$ yarn dev
```

Ahora puede ver la aplicaciòn en el navegador http://localhost:3000
