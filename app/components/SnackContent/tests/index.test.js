import React from 'react';
import { shallow } from 'enzyme';

import { SnackContent } from '../index';

describe('<SnackContent />', () => {
  let onClose;
  let classes;
  let className;

  const styles = {
    success: {
      backgroundColor: '#0000',
    },
    error: {
      backgroundColor: '#8394',
    },
    info: {
      backgroundColor: 'theme.palette.primary.dark',
    },
    warning: {
      backgroundColor: 'amber[700]',
    },
    icon: {
      fontSize: 20,
    },
    iconVariant: {
      opacity: 0.9,
      marginRight: 'theme.spacing.unit',
    },
    message: {
      display: 'flex',
      alignItems: 'center',
    },
  };

  beforeEach(() => {
    classes = styles;
    className = 'className';
    onClose = jest.fn();
  });

  it('should render correctly', () => {
    const wrapper = shallow(
      <SnackContent
        className={className}
        classes={classes}
        message="message"
        variant="success"
        onClose={onClose}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should render variant = error', () => {
    const wrapper = shallow(
      <SnackContent
        className={className}
        classes={classes}
        message="error"
        variant="error"
        onClose={onClose}
      />
    );

    expect(wrapper.length).toEqual(1);
  });

  it('should render variant = warning', () => {
    const wrapper = shallow(
      <SnackContent
        className={className}
        classes={classes}
        message="warning"
        variant="warning"
        onClose={onClose}
      />
    );

    expect(wrapper.length).toEqual(1);
  });

  it('should render variant = info', () => {
    const wrapper = shallow(
      <SnackContent
        className={className}
        classes={classes}
        message="info"
        variant="info"
        onClose={onClose}
      />
    );

    expect(wrapper.length).toEqual(1);
  });
});
