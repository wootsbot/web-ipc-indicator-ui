import React from 'react';
import { shallow } from 'enzyme';

import { MenuAppBar } from '../index';

describe('<MenuAppBar />', () => {
  let singOut;
  let dataAuth;
  let classes;
  let event;

  const styles = {
    root: {
      marginBottom: 50,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
  };

  beforeEach(() => {
    singOut = jest.fn();
    dataAuth = { user: { email: 'jorge' } };
    classes = styles;
    event = { currentTarget: '123' };
  });

  it('should render correctly', () => {
    const wrapper = shallow(
      <MenuAppBar singOut={singOut} dataAuth={dataAuth} classes={classes} />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should change anchorEl state', () => {
    const wrapper = shallow(
      <MenuAppBar singOut={singOut} dataAuth={dataAuth} classes={classes} />
    );
    wrapper.instance().handleMenu(event);
    expect(wrapper.state().anchorEl).toEqual('123');
  });

  it('should change anchorEl to null', () => {
    const wrapper = shallow(
      <MenuAppBar singOut={singOut} dataAuth={dataAuth} classes={classes} />
    );
    wrapper.instance().handleClose();
    expect(wrapper.state().anchorEl).toEqual(null);
  });
});
