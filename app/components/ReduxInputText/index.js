/**
 *
 * ReduxInputText
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

import withReduxAdapter from 'components/ReduxAdapter';

function ReduxInputText(props) {
  const { type, input } = props;

  return (
    <TextField
      {...props}
      value={type === 'password' ? undefined : input.value}
    />
  );
}

ReduxInputText.propTypes = {
  type: PropTypes.string,
  input: PropTypes.object,
};

export default withReduxAdapter(ReduxInputText);
