import React from 'react';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';

import { fieldProps } from '../../ReduxAdapter/tests/constants';
import ReduxInputText from '../index';

describe('<ReduxInputText />', () => {
  it('should render correctly', () => {
    const wrapper = mount(<ReduxInputText {...fieldProps} />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it('should render an input password', () => {
    const wrapper = mount(<ReduxInputText type="password" {...fieldProps} />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
});
