import React from 'react';
import toJSON from 'enzyme-to-json';
import { mount } from 'enzyme';

import withReduxAdapter from '../index';
import { fieldProps } from './constants';

describe('withReduxAdapter', () => {
  let Foo;
  beforeEach(() => {
    //eslint-disable-next-line
    Foo = () => <div />;
  });

  it('should render correctly', () => {
    const WithReduxAdapter = withReduxAdapter(Foo);
    const wrapper = mount(<WithReduxAdapter {...fieldProps} />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it('should render with error prop as string', () => {
    fieldProps.meta.error = 'error generic';
    const WithReduxAdapter = withReduxAdapter(Foo);
    const wrapper = mount(<WithReduxAdapter {...fieldProps} />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
});
