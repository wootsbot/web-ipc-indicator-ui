/**
 *
 * withReduxAdapter
 *
 */

import React from 'react';
import { fieldPropTypes } from 'redux-form';
import wrapDisplayName from 'recompose/wrapDisplayName';

export default function withReduxAdapter(Component) {
  class WithReduxAdapter extends React.PureComponent {
    static propTypes = {
      input: fieldPropTypes.input,
      meta: fieldPropTypes.meta,
    };

    static displayName = wrapDisplayName(Component, 'withReduxAdapter');

    render() {
      const {
        meta: { error, touched, dirty, submitFailed },
        input,
      } = this.props;
      const showError = error && ((touched && dirty) || submitFailed);

      return (
        <Component
          {...this.props}
          {...input}
          id={input.name}
          error={showError}
        />
      );
    }
  }
  return WithReduxAdapter;
}
