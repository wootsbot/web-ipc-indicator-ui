import React from 'react';
import PropTypes from 'prop-types';

import DashboardComponent from 'containers/Dashboard';
import { withLoginProviderHoc } from 'containers/LoginProvider';

class Dashboard extends React.Component {
  static propTypes = {
    dataToken: PropTypes.string,
  };

  componentDidMount() {
    const { dataToken } = this.props;

    if (dataToken === null) {
      window.location.replace('/');
    }
  }

  render() {
    const { dataToken } = this.props;

    if (dataToken === null) {
      return null;
    }

    return <DashboardComponent />;
  }
}

export default withLoginProviderHoc(Dashboard);
