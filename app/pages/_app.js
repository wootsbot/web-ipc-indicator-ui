import React from 'react';

import { Provider } from 'react-redux';
import JssProvider from 'react-jss/lib/JssProvider';

import NextApp, { Container } from 'next/app';
import Head from 'next/head';

import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import App from 'containers/App';

import getPageContext from 'utils/get-page-context';
import withReduxStore from 'utils/withReduxStore';

import 'utils/moment-config';

import firebase from 'firebase/app';

const config = {
  apiKey: 'AIzaSyAn1qoz0F11eRB36d8Ki5ljuPmJQ215uH0',
  authDomain: 'web-ipc-indicator-ui.firebaseapp.com',
  databaseURL: 'https://web-ipc-indicator-ui.firebaseio.com',
  projectId: 'web-ipc-indicator-ui',
  storageBucket: 'web-ipc-indicator-ui.appspot.com',
  messagingSenderId: '370881427744',
};

class Srr extends NextApp {
  constructor() {
    super();
    this.pageContext = getPageContext();
  }

  componentDidMount() {
    firebase.initializeApp(config);

    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;
    return (
      <Container>
        <Head>
          <title>My page</title>
        </Head>
        <JssProvider
          registry={this.pageContext.sheetsRegistry}
          generateClassName={this.pageContext.generateClassName}>
          <MuiThemeProvider
            theme={this.pageContext.theme}
            sheetsManager={this.pageContext.sheetsManager}>
            <CssBaseline />

            <Provider store={reduxStore}>
              <App>
                <Component pageContext={this.pageContext} {...pageProps} />
              </App>
            </Provider>
          </MuiThemeProvider>
        </JssProvider>
      </Container>
    );
  }
}

export default withReduxStore(Srr);
