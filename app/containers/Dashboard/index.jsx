import React from 'react';
import PropTypes from 'prop-types';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { Line } from 'react-chartjs-2';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import MenuAppBar from 'components/MenuAppBar';

import saga from './saga';
import reducer from './reducer';
import { getDataGraphs } from './actions';
import {
  selectDataGraphs,
  selectDataLoading,
  selectDataError,
} from './selectors';

import { getDataSets } from './utils';

export class Dashboard extends React.Component {
  static propTypes = {
    getDataGraphs: PropTypes.func,
    dataGraphs: PropTypes.array,
    dataLoading: PropTypes.bool,
    dataError: PropTypes.bool,
  };

  componentDidMount() {
    const { getDataGraphs } = this.props;

    getDataGraphs();
  }

  render() {
    const { dataGraphs, dataLoading, dataError } = this.props;

    return (
      <React.Fragment>
        <MenuAppBar />
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid container item sm={6}>
            <Paper style={{ width: '100%', padding: 24 }}>
              {dataLoading && <CircularProgress color="secondary" />}

              <Line data={getDataSets(dataGraphs)} />

              {dataError && (
                <Typography component="h2" variant="h1" gutterBottom>
                  Error no pudimos cargar la grafica
                </Typography>
              )}
            </Paper>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  dataGraphs: selectDataGraphs,
  dataLoading: selectDataLoading,
  dataError: selectDataError,
});

export function mapDispatchToProps(dispatch) {
  return { getDataGraphs: () => dispatch(getDataGraphs()) };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withSaga = injectSaga({ key: 'dashboard', saga });
const withReducer = injectReducer({ key: 'dashboard', reducer });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Dashboard);
