import moment from 'moment-timezone';

const DATA_SETS = {
  label: 'IPC indicator',
  fill: true,
  lineTension: 0.3,
  backgroundColor: 'rgba(75,192,192,0.4)',
  borderColor: 'rgba(75,192,192,1)',
  borderCapStyle: 'butt',
  borderDash: [],
  borderDashOffset: 0.0,
  borderJoinStyle: 'miter',
  pointBorderColor: 'rgba(75,192,192,1)',
  pointBackgroundColor: '#fff',
  pointBorderWidth: 2,
  pointHoverRadius: 5,
  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
  pointHoverBorderColor: 'rgba(220,220,220,1)',
  pointHoverBorderWidth: 2,
  pointRadius: 5,
  pointHitRadius: 10,
  data: [],
};

export function getLabels(data) {
  let labels = [];

  data.forEach(item => {
    let itemDate = moment(item.fecha).format('ll LTS');

    labels.push(itemDate);
  });

  return labels;
}

export function getData(data) {
  let dataPrice = [];

  data.forEach(item => {
    let itemData = item.precio;

    dataPrice.push(itemData);
  });

  return dataPrice;
}

export function getDataSets(data) {
  let objectData;
  const labels = getLabels(data);
  const dataPrice = getData(data);

  DATA_SETS.data = dataPrice;
  objectData = { labels, datasets: [DATA_SETS] };

  return objectData;
}
