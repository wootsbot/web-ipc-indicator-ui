import { createSelector } from 'reselect';

export const selectGraphsDomain = state => state.dashboard;

export const selectDataGraphs = createSelector(
  selectGraphsDomain,
  subState => (subState && subState.dataGraphs.data) || []
);

export const selectDataLoading = createSelector(
  selectGraphsDomain,
  subState => subState.loading
);

export const selectDataError = createSelector(
  selectGraphsDomain,
  subState => Boolean(subState.error)
);
