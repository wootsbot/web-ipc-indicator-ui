import { takeLatest, put, call } from 'redux-saga/effects';

import baasRequest from 'utils/baasRequest';
import { getDataRequest } from 'utils/api/baas/data-graphs';

import { getDataGraphs } from './actions';

export function* getDataGraphsRequest() {
  try {
    yield put(getDataGraphs.request());

    const data = yield call(baasRequest, getDataRequest, {});

    yield put(getDataGraphs.success(data));
  } catch (err) {
    yield put(getDataGraphs.failure(err));
  } finally {
    yield put(getDataGraphs.fulfill());
  }
}

export default function* dataGraphs() {
  yield takeLatest(getDataGraphs.TRIGGER, getDataGraphsRequest);
}
