import { createRoutine } from 'redux-saga-routines';

import { GET_DATA_GRAPHS } from './constants';

export const getDataGraphs = createRoutine(GET_DATA_GRAPHS);
