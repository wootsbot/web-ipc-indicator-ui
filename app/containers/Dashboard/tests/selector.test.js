import {
  selectDataGraphs,
  selectDataLoading,
  selectDataError,
} from '../selectors';

describe('selectors', () => {
  const state = {
    dashboard: {
      loading: false,
      loadend: false,
      error: null,
      dataGraphs: [],
    },
  };

  it('should return state data', () => {
    const stateDefault = {
      dashboard: {
        ...state,
        dataGraphs: { data: [{ key: '123' }] },
      },
    };

    const expected = {
      dashboard: {
        loading: false,
        loadend: false,
        error: null,
        dataGraphs: { data: [{ key: '123' }] },
      },
    };

    expect(selectDataGraphs(stateDefault)).toEqual(
      expected.dashboard.dataGraphs.data
    );
  });

  it('should return state data null', () => {
    expect(selectDataGraphs(state)).toEqual([]);
  });

  it('should return state loading', () => {
    expect(selectDataLoading(state)).toEqual(state.dashboard.loading);
  });

  it('should return state loading', () => {
    expect(selectDataError(state)).toEqual(false);
  });
});
