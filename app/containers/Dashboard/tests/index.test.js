import React from 'react';
import { shallow } from 'enzyme';

import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import { Dashboard, mapDispatchToProps } from '../index';
import { getDataGraphs } from '../actions';

describe('<Dashboard />', () => {
  let getDataGraphsMock;
  let dataGraphs;
  let dataLoading;
  let dataError;

  beforeEach(() => {
    getDataGraphsMock = jest.fn();
    dataGraphs = [
      {
        fecha: '2019-01-11T08:31:34.063-06:00',
        precio: 43727.52,
        porcentaje: 0.13,
        volumen: 332306,
      },
    ];
    dataLoading = false;
    dataError = false;
  });

  it('should render correctly', () => {
    const component = shallow(
      <Dashboard
        getDataGraphs={getDataGraphsMock}
        dataGraphs={dataGraphs}
        dataLoading={dataLoading}
        dataError={dataError}
      />
    );

    expect(component.length).toBe(1);
  });

  it('should render CircularProgress in leoding', () => {
    const component = shallow(
      <Dashboard
        getDataGraphs={getDataGraphsMock}
        dataGraphs={dataGraphs}
        dataLoading
        dataError={dataError}
      />
    );

    expect(component.find(CircularProgress).exists()).toBe(true);
  });

  it('should render Typography in error', () => {
    const component = shallow(
      <Dashboard
        getDataGraphs={getDataGraphsMock}
        dataGraphs={dataGraphs}
        dataLoading={dataLoading}
        dataError
      />
    );

    expect(component.find(Typography).exists()).toBe(true);
  });

  describe('mapDispatchToProps', () => {
    let dispatch;

    beforeEach(() => {
      dispatch = jest.fn();
    });

    it('should call getDataGraphs action', () => {
      const props = mapDispatchToProps(dispatch);
      props.getDataGraphs();
      expect(dispatch).toHaveBeenCalledWith(getDataGraphs());
    });
  });
});
