/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */

import { takeLatest } from 'redux-saga/effects';
import { testSaga } from 'redux-saga-test-plan';
import baasRequest from 'utils/baasRequest';

import { getDataRequest } from 'utils/api/baas/data-graphs';

import dataGraphs, { getDataGraphsRequest } from '../saga';
import { getDataGraphs } from '../actions';

describe('Saga', () => {
  const mainSaga = dataGraphs();

  it('should get list of getDataGraphs', () => {
    const takeLatestDescriptor = mainSaga.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(getDataGraphs.TRIGGER, getDataGraphsRequest)
    );
  });

  describe('getRecipients actions', () => {
    it('should getDataGraphsRequest() be success', () => {
      testSaga(getDataGraphsRequest)
        .next()
        .put(getDataGraphs.request())
        .next()
        .call(baasRequest, getDataRequest, {})
        .next({ code: 'ok', data: [{ ket: '123' }] })
        .put(getDataGraphs.success({ code: 'ok', data: [{ ket: '123' }] }))
        .next()
        .put(getDataGraphs.fulfill())
        .next()
        .isDone();
    });

    it('should getDataGraphsRequest() be failure', () => {
      testSaga(getDataGraphsRequest)
        .next()
        .put(getDataGraphs.request())
        .next()
        .call(baasRequest, getDataRequest, {})
        .throw('error')
        .put(getDataGraphs.failure('error'))
        .next()
        .put(getDataGraphs.fulfill())
        .next()
        .isDone();
    });
  });
});
