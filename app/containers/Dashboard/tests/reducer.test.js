import dashboardReducer from '../reducer';
import { getDataGraphs } from '../actions';

describe('dashboardReducer', () => {
  let state;

  beforeEach(() => {
    state = {
      loading: false,
      loadend: false,
      error: null,
      dataGraphs: [],
    };
  });

  it('should return the initial state', () => {
    expect(dashboardReducer(undefined, {})).toEqual(state);
  });

  describe('getDataGraphs actions', () => {
    it('should handle the action getDataGraphs.REQUEST correctly', () => {
      const expectResult = {
        ...state,
        loading: true,
      };

      expect(dashboardReducer(state, getDataGraphs.request())).toEqual(
        expectResult
      );
    });

    it('should return the action getDataGraphs.SUCCESS correctly', () => {
      const expectResult = {
        ...state,
        loading: false,
        loadend: true,
        dataGraphs: [{ key: '123' }],
      };

      expect(
        dashboardReducer(state, getDataGraphs.success([{ key: '123' }]))
      ).toEqual(expectResult);
    });

    it('should return the action getDataGraphs.FAILURE', () => {
      const expectResult = {
        ...state,
        loading: false,
        loadend: false,
        error: 'error',
      };

      expect(dashboardReducer(state, getDataGraphs.failure('error'))).toEqual(
        expectResult
      );
    });
  });
});
