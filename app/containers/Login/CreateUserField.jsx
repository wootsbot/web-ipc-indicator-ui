import React from 'react';
import PropTypes from 'prop-types';

import { Field } from 'redux-form';
import Link from 'next/link';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

import ReduxInputText from 'components/ReduxInputText';

import styles from './index.scss';

function CreateUserField({ loading, onAction }) {
  return (
    <React.Fragment>
      <Grid container item md={12}>
        <Field
          id="outlined-email-input2"
          label="Correo Electronico"
          type="email"
          name="create_email"
          autoComplete="email"
          margin="normal"
          variant="outlined"
          style={{ width: '100%' }}
          component={ReduxInputText}
        />
      </Grid>

      <Grid container item md={12}>
        <Field
          id="outlined-password-input2"
          label="Contraseña"
          type="password"
          name="create_password"
          autoComplete="current-password"
          margin="normal"
          variant="outlined"
          style={{ width: '100%' }}
          component={ReduxInputText}
        />
      </Grid>

      <Grid
        className={styles.buttons}
        container
        item
        md={12}
        direction="row"
        justify="space-between"
        alignItems="center">
        <Link prefetch href="/">
          <Button color="secondary" onClick={onAction}>
            Cancelar
          </Button>
        </Link>
        <Button type="submit" variant="contained" color="primary">
          Crear Usuario
          {loading && <CircularProgress size={25} color="secondary" />}
        </Button>
      </Grid>
    </React.Fragment>
  );
}

CreateUserField.propTypes = {
  loading: PropTypes.bool,
  onAction: PropTypes.func,
};

export default CreateUserField;
