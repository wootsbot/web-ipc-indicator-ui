import React from 'react';
import PropTypes from 'prop-types';

import { Field } from 'redux-form';
import Link from 'next/link';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

import ReduxInputText from 'components/ReduxInputText';

import styles from './index.scss';

function LoginField({ loading, onAction }) {
  return (
    <React.Fragment>
      <Grid container item md={12}>
        <Field
          id="outlined-email-input"
          label="Correo Electronico"
          type="email"
          name="email"
          autoComplete="email"
          margin="normal"
          variant="outlined"
          style={{ width: '100%' }}
          component={ReduxInputText}
        />
      </Grid>

      <Grid container item md={12}>
        <Field
          id="outlined-password-input"
          label="Contraseña"
          type="password"
          name="password"
          autoComplete="current-password"
          margin="normal"
          variant="outlined"
          style={{ width: '100%' }}
          component={ReduxInputText}
        />
      </Grid>

      <Grid
        className={styles.buttons}
        container
        item
        md={12}
        direction="row"
        justify="space-between"
        alignItems="center">
        <Link prefetch href="/">
          <Button color="secondary" onClick={onAction}>
            Crear cuenta
          </Button>
        </Link>
        <Button type="submit" variant="contained" color="primary">
          Acceder
          {loading && <CircularProgress size={25} color="secondary" />}
        </Button>
      </Grid>
    </React.Fragment>
  );
}

LoginField.propTypes = {
  loading: PropTypes.bool,
  onAction: PropTypes.func,
};

export default LoginField;
