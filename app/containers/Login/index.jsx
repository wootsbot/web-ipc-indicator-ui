import React from 'react';
import PropTypes from 'prop-types';

import Snackbar from '@material-ui/core/Snackbar';

import { withLoginProviderHoc } from 'containers/LoginProvider';
import { typesSnackBarClose } from 'containers/LoginProvider/constants';
import SnackContent from 'components/SnackContent';

import LoginForm from './LoginForm';
import CreateUserForm from './CreateUserForm';

class Login extends React.Component {
  static propTypes = {
    dataAuth: PropTypes.object,
    singUserFormHandler: PropTypes.func,
    closeSnackBar: PropTypes.func,
    createUserForm: PropTypes.func,
    dataCreateUser: PropTypes.object,
  };

  render() {
    const {
      dataAuth,
      singUserFormHandler,
      closeSnackBar,
      dataCreateUser,
      createUserForm,
    } = this.props;

    return (
      <React.Fragment>
        {dataCreateUser.create ? (
          <CreateUserForm
            onAction={createUserForm}
            loading={dataAuth.fetching}
            singUserFormHandler={singUserFormHandler}
          />
        ) : (
          <LoginForm
            onAction={createUserForm}
            loading={dataAuth.fetching}
            singUserFormHandler={singUserFormHandler}
          />
        )}

        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={Boolean(dataCreateUser.error)}
          autoHideDuration={6000}
          onClose={() =>
            closeSnackBar(typesSnackBarClose.CREATE_USER_CLOSE_SNACK_ERROR)
          }>
          <SnackContent
            onClose={() =>
              closeSnackBar(typesSnackBarClose.CREATE_USER_CLOSE_SNACK_ERROR)
            }
            variant="error"
            message={dataCreateUser.error && dataCreateUser.error.message}
          />
        </Snackbar>
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={Boolean(dataCreateUser.fetched)}
          autoHideDuration={6000}
          onClose={() =>
            closeSnackBar(typesSnackBarClose.CREATE_USER_CLOSE_SNACK_SUCCESS)
          }>
          <SnackContent
            onClose={() =>
              closeSnackBar(typesSnackBarClose.CREATE_USER_CLOSE_SNACK_SUCCESS)
            }
            variant="success"
            message="La cuenta se creo correctamente"
          />
        </Snackbar>
      </React.Fragment>
    );
  }
}

export default withLoginProviderHoc(Login);
