export function validateLoginForm(values) {
  const errors = {};
  const { email, password } = values;

  if (!email) {
    errors.email = 'escribir correo';
  }

  if (!password) {
    errors.password = 'escribir contraseña';
  }

  return errors;
}

export function validateCreateForm(values) {
  const errors = {};
  const { create_email, create_password } = values;

  if (!create_email) {
    errors.create_email = 'escribir correo';
  }

  if (!create_password) {
    errors.create_password = 'escribir contraseña';
  }

  return errors;
}
