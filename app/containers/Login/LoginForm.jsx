import React from 'react';
import PropTypes from 'prop-types';

import { Form, reduxForm } from 'redux-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { singUserFormHandler } from 'containers/LoginProvider/actions';

import LoginField from './LoginField';

import styles from './index.scss';
import { validateLoginForm as validate } from './validators';

export class LoginForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    onAction: PropTypes.func,
    loading: PropTypes.bool,
  };

  render() {
    const { handleSubmit, loading, onAction } = this.props;

    return (
      <Form onSubmit={handleSubmit(singUserFormHandler)}>
        <Grid
          className={styles.root}
          container
          direction="column"
          justify="center"
          alignItems="center">
          <Grid
            item
            className={styles.container}
            container
            direction="column"
            justify="center"
            alignItems="center"
            sm={3}>
            <Grid
              item
              md={12}
              container
              direction="column"
              justify="center"
              alignItems="center">
              <img
                src="/static/logo.png"
                alt="my image"
                className={styles.logo}
              />
              <Typography variant="h5" gutterBottom>
                Acceder
              </Typography>
            </Grid>
            <LoginField loading={loading} onAction={onAction} />
          </Grid>
        </Grid>
      </Form>
    );
  }
}

export default reduxForm({
  validate,
  form: 'loginForm',
})(LoginForm);
