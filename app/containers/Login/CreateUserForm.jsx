import React from 'react';
import PropTypes from 'prop-types';

import { Form, reduxForm } from 'redux-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { createUserFormHandler } from 'containers/LoginProvider/actions';

import CreateUserField from './CreateUserField';

import styles from './index.scss';
import { validateCreateForm as validate } from './validators';

export class CreateUserForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    onAction: PropTypes.func,
    loading: PropTypes.bool,
  };

  render() {
    const { handleSubmit, onAction } = this.props;

    return (
      <Form onSubmit={handleSubmit(createUserFormHandler)}>
        <Grid
          className={styles.root}
          container
          direction="column"
          justify="center"
          alignItems="center">
          <Grid
            item
            className={styles.container}
            container
            direction="column"
            justify="center"
            alignItems="center"
            sm={3}>
            <Grid
              item
              md={12}
              container
              direction="column"
              justify="center"
              alignItems="center">
              <img
                src="/static/logo.png"
                alt="my image"
                className={styles.logo}
              />
              <Typography variant="h5" gutterBottom>
                Crear cuenta
              </Typography>
            </Grid>
            <CreateUserField onAction={onAction} />
          </Grid>
        </Grid>
      </Form>
    );
  }
}

export default reduxForm({
  validate,
  form: 'createUserForm',
})(CreateUserForm);
