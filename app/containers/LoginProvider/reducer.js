import {
  singOut,
  singUserSubmit,
  createUser,
  createUserSubmit,
} from './actions';

import {
  CLOSE_SNACK_BAR,
  typesSnackBarClose,
  CREATE_USER_FORM,
} from './constants';

const initialState = {
  singOut: {
    fetching: false,
    fetched: false,
    error: null,
  },
  auth: {
    fetching: false,
    fetched: false,
    error: null,
    user: null,
  },
  createUser: {
    fetching: false,
    fetched: false,
    requested: false,
    error: null,
    create: false,
  },
};

function loginReducer(state = initialState, { type, payload }) {
  switch (type) {
    case singUserSubmit.REQUEST:
      return {
        ...state,
        auth: { ...state.auth, fetching: true, fetched: false, error: null },
      };

    case singUserSubmit.SUCCESS:
      return {
        ...state,
        auth: {
          ...state.auth,
          fetching: false,
          fetched: true,
          error: null,
          user: payload,
        },
      };

    case singUserSubmit.FAILURE:
      return {
        ...state,
        auth: {
          ...state.auth,
          fetching: false,
          fetched: false,
          error: payload,
          user: null,
        },
      };

    case singOut.REQUEST:
      return {
        ...state,
        singOut: {
          ...state.singOut,
          fetching: true,
          fetched: false,
          error: null,
        },
      };

    case singOut.SUCCESS:
      return {
        ...state,
        singOut: {
          ...state.singOut,
          fetching: false,
          fetched: true,
          error: null,
        },
        auth: {
          ...state.auth,
          fetching: false,
          fetched: false,
          error: false,
          user: null,
        },
      };

    case singOut.FAILURE:
      return {
        ...state,
        singOut: {
          ...state.singOut,
          fetching: false,
          fetched: false,
          error: payload,
        },
      };

    case createUser.REQUEST:
      return {
        ...state,
        createUser: {
          ...state.createUser,
          fetching: true,
          fetched: false,
          error: null,
        },
      };

    case createUser.FAILURE:
      return {
        ...state,
        createUser: {
          ...state.createUser,
          fetching: false,
          fetched: false,
          error: payload,
        },
      };

    case createUserSubmit.REQUEST:
      return {
        ...state,
        createUser: {
          ...state.createUser,
          fetching: true,
          fetched: false,
          requested: false,
          error: null,
        },
      };

    case createUserSubmit.SUCCESS:
      return {
        ...state,
        createUser: {
          ...state.createUser,
          fetching: false,
          fetched: true,
          create: false,
          error: null,
        },
      };

    case createUserSubmit.FAILURE:
      return {
        ...state,
        createUser: {
          ...state.createUser,
          fetching: false,
          fetched: false,
          requested: false,
          error: payload,
        },
      };

    case CLOSE_SNACK_BAR: {
      if (payload === typesSnackBarClose.CREATE_USER_CLOSE_SNACK_ERROR) {
        return {
          ...state,
          createUser: {
            ...state.createUser,
            fetching: false,
            fetched: false,
            error: null,
          },
        };
      }

      if (payload === typesSnackBarClose.CREATE_USER_CLOSE_SNACK_SUCCESS) {
        return {
          ...state,
          createUser: {
            ...state.createUser,
            fetching: false,
            fetched: false,
            error: null,
          },
        };
      }

      return { ...state, createUser: { ...state.createUser } };
    }

    case CREATE_USER_FORM:
      return {
        ...state,
        createUser: {
          ...state.createUser,
          create: !state.createUser.create,
        },
      };

    default:
      return state;
  }
}

export default loginReducer;
