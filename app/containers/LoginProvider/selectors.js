import { createSelector } from 'reselect';

export const selectLoginProviderDomain = state => state.loginProvider;

export const selectDataAuth = createSelector(
  selectLoginProviderDomain,
  subState => (subState && subState.auth) || []
);

export const selectToken = createSelector(
  selectLoginProviderDomain,
  subState => (subState.auth.user && subState.auth.user.refreshToken) || null
);

export const selectCreateUser = createSelector(
  selectLoginProviderDomain,
  subState => subState.createUser
);
