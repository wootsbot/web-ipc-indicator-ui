import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import compose from 'recompose/compose';
import wrapDisplayName from 'recompose/wrapDisplayName';

import { singOut, closeSnackBar, createUserForm } from './actions';
import * as selectors from './selectors';

export function withLoginProviderHoc(Component) {
  class WithSuggestions extends React.Component {
    static displayName = wrapDisplayName(Component, 'withSearch');
    render() {
      return <Component {...this.props} />;
    }
  }

  return WithSuggestions;
}

const makeMapStateToProps = () => {
  const selector = createStructuredSelector({
    dataAuth: selectors.selectDataAuth,
    dataCreateUser: selectors.selectCreateUser,
    dataToken: selectors.selectToken,
  });

  const mapStateToProps = (state, props) => selector(state, props);

  return mapStateToProps;
};

export function mapDispatchToProps(dispatch) {
  return {
    singOut: () => dispatch(singOut()),
    closeSnackBar: type => dispatch(closeSnackBar(type)),
    createUserForm: () => dispatch(createUserForm()),
  };
}

export const withLoginProviderState = connect(
  makeMapStateToProps,
  mapDispatchToProps
);

export default compose(
  withLoginProviderState,
  withLoginProviderHoc
);
