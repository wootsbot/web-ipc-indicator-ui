export const SING_USER = 'app/LoginProvider/SING_USER';
export const SING_USER_SUBMIT = 'app/LoginProvider/SING_USER_SUBMIT';

export const SIGN_OUT = 'app/LoginProvider/SIGN_OUT';

export const CREATE_USER = 'app/LoginProvider/CREATE_USER';
export const CREATE_USER_SUBMIT = 'app/LoginProvider/CREATE_USER_SUBMIT';

export const CLOSE_SNACK_BAR = 'app/LoginProvider/CLOSE_SNACK_BAR';

export const CREATE_USER_FORM = 'app/LoginProvider/CREATE_USER_FORM';

const CREATE_USER_CLOSE_SNACK_ERROR = 'create_user_close_snack_error';
const CREATE_USER_CLOSE_SNACK_SUCCESS = 'create_user_close_snack_success';

export const typesSnackBarClose = {
  CREATE_USER_CLOSE_SNACK_ERROR,
  CREATE_USER_CLOSE_SNACK_SUCCESS,
};
