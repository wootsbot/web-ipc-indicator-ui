import { takeLatest, put, call, select } from 'redux-saga/effects';
import Router from 'next/router';

import {
  firebaseAuth,
  firebaseCreateUser,
  firebaseSingOut,
} from 'utils/api/baas/login';

import {
  singOut,
  singUser,
  singUserSubmit,
  createUser,
  createUserSubmit,
} from './actions';
import { selectToken } from './selectors';

export async function pushDashboard() {
  await Router.push('/Dashboard');
}

export async function pushHome() {
  await Router.push('/');
}

export function* singOutRequest() {
  try {
    yield put(singOut.request());

    yield call(firebaseSingOut);

    yield put(singOut.success());
  } catch (err) {
    yield put(singOut.failure(err));
  } finally {
    yield put(singOut.fulfill());
  }
}

export function* submitDashboard() {
  try {
    const accessToken = yield select(selectToken);

    if (accessToken) {
      yield call(pushDashboard);
    }
  } catch (err) {
    yield put(singUserSubmit.failure(err));
  } finally {
    yield put(singUserSubmit.fulfill());
  }
}

export function* submitLoginRequest(values) {
  try {
    yield put(singUserSubmit.request());

    const response = yield call(firebaseAuth, values);

    yield put(singUserSubmit.success(response));
  } catch (err) {
    yield put(singUserSubmit.failure(err));
  } finally {
    yield put(singUserSubmit.fulfill());
  }
}

export function* validateLoginEditionForm(action) {
  const { payload } = action;
  const { props, values } = payload;

  if (props.validExceptSubmit) {
    yield call(submitLoginRequest, values);
  } else {
    yield put(singUser.failure());
  }
}

export function* submitCreateUserRequest(values) {
  try {
    yield put(createUserSubmit.request());

    const response = yield call(firebaseCreateUser, values);

    yield put(createUserSubmit.success(response));
  } catch (err) {
    yield put(createUserSubmit.failure(err));
  } finally {
    yield put(createUserSubmit.fulfill());
  }
}

export function* validateCreateUserEditionForm(action) {
  const { payload } = action;
  const { props, values } = payload;

  if (props.validExceptSubmit) {
    yield call(submitCreateUserRequest, values);
  } else {
    yield put(createUser.failure());
  }
}

export default function* auth() {
  yield takeLatest(singOut.TRIGGER, singOutRequest);
  yield takeLatest(singOut.SUCCESS, pushHome);

  yield takeLatest(singUser.TRIGGER, validateLoginEditionForm);
  yield takeLatest(singUserSubmit.SUCCESS, submitDashboard);

  yield takeLatest(createUser.TRIGGER, validateCreateUserEditionForm);
}
