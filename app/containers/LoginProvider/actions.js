import { createRoutine, bindRoutineToReduxForm } from 'redux-saga-routines';

import {
  SIGN_OUT,
  SING_USER,
  SING_USER_SUBMIT,
  CREATE_USER,
  CREATE_USER_SUBMIT,
  CLOSE_SNACK_BAR,
  CREATE_USER_FORM,
} from './constants';

export const singOut = createRoutine(SIGN_OUT);

export const singUser = createRoutine(SING_USER);
export const singUserFormHandler = bindRoutineToReduxForm(singUser);
export const singUserSubmit = createRoutine(SING_USER_SUBMIT);

export const createUser = createRoutine(CREATE_USER);
export const createUserFormHandler = bindRoutineToReduxForm(createUser);
export const createUserSubmit = createRoutine(CREATE_USER_SUBMIT);

export function closeSnackBar(type) {
  return { type: CLOSE_SNACK_BAR, payload: type };
}

export function createUserForm() {
  return { type: CREATE_USER_FORM };
}
