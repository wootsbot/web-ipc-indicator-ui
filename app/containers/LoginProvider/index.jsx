import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import reducer from './reducer';
import saga from './saga';
export { default as withLoginProviderHoc } from './withLoginProviderHoc';

export class LoginProvider extends React.Component {
  render() {
    const { children } = this.props;
    return React.Children.only(children);
  }
}

LoginProvider.propTypes = {
  children: PropTypes.element.isRequired,
};

const withConnect = connect();
const withReducer = injectReducer({ key: 'loginProvider', reducer });
const withSaga = injectSaga({ key: 'loginProvider', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(LoginProvider);
