import React from 'react';
import PropTypes from 'prop-types';

import LoginProvider from 'containers/LoginProvider';

export class AppProviders extends React.Component {
  render() {
    const { children } = this.props;

    return <LoginProvider>{React.Children.only(children)}</LoginProvider>;
  }
}

AppProviders.propTypes = {
  children: PropTypes.element.isRequired,
};

export default AppProviders;
