import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';

import AppProviders from '../Providers';

describe('<AppProviders />', () => {
  it('should render correctly', () => {
    const Foo = <div />;
    const renderedComponent = shallow(
      <AppProviders>
        <Foo />
      </AppProviders>
    );
    expect(toJSON(renderedComponent)).toMatchSnapshot();
  });
});
