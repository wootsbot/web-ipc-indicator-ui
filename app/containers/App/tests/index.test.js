import React from 'react';
import toJSON from 'enzyme-to-json';
import { shallow } from 'enzyme';

import App from '../index';

describe('App', () => {
  let Foo;
  beforeEach(() => {
    //eslint-disable-next-line
    Foo = () => <div />;
  });

  it('should render correctly', () => {
    const wrapper = shallow(
      <App>
        <Foo />
      </App>
    );
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
});
