import React from 'react';

import AppProviders from './Providers';
//eslint-disable-next-line
function App({ children }) {
  return <AppProviders>{React.Children.only(children)}</AppProviders>;
}

export default App;
