import firebase from 'firebase/app';
import 'firebase/auth';

export async function firebaseAuth(values) {
  const response = await firebase
    .auth()
    .signInWithEmailAndPassword(values.email, values.password);
  return response.user;
}

export async function firebaseCreateUser(values) {
  const response = await firebase
    .auth()
    .createUserWithEmailAndPassword(
      values.create_email,
      values.create_password
    );
  return response;
}

export async function firebaseSingOut() {
  const response = await firebase.auth().signOut();
  return response;
}
