import request from 'utils/request';

export function getDataRequest(options = {}) {
  return request(
    `https://us-central1-web-ipc-indicator-ui.cloudfunctions.net/getDataGraphs`,
    {
      method: 'GET',
      ...options,
    }
  );
}
