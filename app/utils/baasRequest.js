import { call, put } from 'redux-saga/effects';

export const createHeaders = () => ({
  'Client-Screen-Resolution': `${window.screen.width}x${window.screen.height}`,
  'Browser-Time-Zone':
    window.Intl.DateTimeFormat().resolvedOptions().timeZone ||
    new Date().toString('en').match(/\(.+\)/)[0],
  'Content-Type': 'application/json',
  'Channel-Id': '1bbfa1e8-6b0a-4a4e-9158-3f24edbfe006',
});

export default function* baasRequest(request, ...params) {
  const headers = createHeaders();
  const credentials = 'same-origin';

  try {
    const response = yield call(request, ...params, { credentials, headers });
    return response;
  } catch (err) {
    if (err.response && err.response.status === 401) {
      yield put('/');
    }

    throw err;
  }
}
