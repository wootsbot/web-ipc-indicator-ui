import 'moment/locale/es';
import moment from 'moment-timezone';

moment.locale('es');
moment.tz.setDefault('America/Mexico_City');
