export const themeUI = {
  palette: {
    primary: { light: '#4d4d4d', main: '#212121', dark: '#171717' },
    secondary: { light: '#ffcf33', main: '#ffc400', dark: '#b28900' },
  },
  typography: { useNextVariants: true },
};
