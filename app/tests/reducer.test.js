import appReducer from '../reducer';

describe('app reducer', () => {
  it('should return initial app state', () => {
    const state = { loading: false, error: false };

    const reducer = appReducer();
    expect(reducer).toEqual(state);
  });
});
